# encoding: UTF-8
# rubocop:disable LineLength
#
# Cookbook Name:: wildfly
# Recipe:: mysql_connector
#
# Copyright (C) 2014 Brian Dwyer - Intelligent Digital Services
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


# => Shorten Hashes
wildfly = node['wildfly']

# => Shorten Module Directory Name
modules_dir = ::File.join(wildfly['base'], 'modules', 'org', 'vendaweb', 'main')
jars = %w(antlr-2.7.7.jar aopalliance.jar AS400Comm.jar aspectjrt.jar aspectjtools-1.6.6.jar axis-1.4.jar
          c3p0-0.9.1.2.jar cglib-nodep-2.1_3.jar commons-codec-1.2.jar commons-collections-3.2.1.jar
          commons-discovery-0.4.jar commons-httpclient-3.1.jar commons-lang-2.0.jar ConsumidorVentaWebService.jar
          guava-r07.jar hibernate-commons-annotations.jar hibernate-core.jar iogi-0.9.1.jar javassist-3.14.0.GA.jar 
          jna-4.1.0.jar jna-platform-4.1.0.jar jt400.jar junit-4.12.jar LibSiTef.jar log4j-1.2.16.jar mail-1.3.3.jar
          mirror-1.5.1.jar objenesis-1.1.jar org.springframework.aop-3.1.1.RELEASE.jar org.springframework.asm-3.1.1.RELEASE.jar
          org.springframework.aspects-3.1.1.RELEASE.jar org.springframework.beans-3.1.1.RELEASE.jar
          org.springframework.context-3.1.1.RELEASE.jar org.springframework.context.support-3.1.1.RELEASE.jar
          org.springframework.core-3.1.1.RELEASE.jar org.springframework.expression-3.1.1.RELEASE.jar
          org.springframework.instrument-3.1.1.RELEASE.jar org.springframework.instrument.tomcat-3.1.1.RELEASE.jar
          org.springframework.jdbc-3.1.1.RELEASE.jar org.springframework.jms-3.1.1.RELEASE.jar 
          org.springframework.orm-3.1.1.RELEASE.jar org.springframework.oxm-3.1.1.RELEASE.jar
          org.springframework.test-3.1.1.RELEASE.jar org.springframework.transaction-3.1.1.RELEASE.jar
          org.springframework.web-3.1.1.RELEASE.jar org.springframework.web.portlet-3.1.1.RELEASE.jar
          org.springframework.web.servlet-3.1.1.RELEASE.jar org.springframework.web.struts-3.1.1.RELEASE.jar
          paranamer-2.2.jar paypal-core-1.6.2.jar quartz-1.8.6.jar scannotation-1.0.3.jar spring-security-acl-3.0.7.RELEASE.jar
          spring-security-aspects-3.0.7.RELEASE.jar spring-security-cas-client-3.0.7.RELEASE.jar
          spring-security-config-3.0.7.RELEASE.jar spring-security-core-3.0.7.RELEASE.jar spring-security-ldap-3.0.7.RELEASE.jar
          spring-security-taglibs-3.0.7.RELEASE.jar spring-security-web-3.0.7.RELEASE.jar sun.misc.BASE64Decoder.jar
          validation-api-1.0.0.GA.jar xpp3_min-1.1.4c.jar xstream-1.3.1.jar)

dependencies = %w(javaee.api org.apache.commons.logging org.jboss.vfs org.hibernate org.dom4j org.slf4j
javax.el.api com.sun.xml.bind)

# => Create Modules Directory
directory modules_dir do
  owner wildfly['user']
  group wildfly['group']
  mode 0755
  recursive true
end

# => Download MySQL Connector/J Tarball
remote_file "#{Chef::Config[:file_cache_path]}/modules.tar.gz" do
  source wildfly['modules']
  action :create
  notifies :run, 'bash[Extract Modules]', :immediately
end

# => Extract Modules
bash 'Extract Modules' do
  cwd Chef::Config[:file_cache_path]
  code <<-EOF
  tar xzf modules.tar.gz -C #{modules_dir} --strip 1
  chown #{wildfly['user']}:#{wildfly['group']} -R #{modules_dir}/../../
  EOF
  action :nothing
end

# => Configure Module
template ::File.join(modules_dir, 'module.xml') do
  source 'module.xml.erb'
  user wildfly['user']
  group wildfly['group']
  mode '0644'
  variables(
    module_name: 'org.vendaweb',
    resources_path: jars,
    module_dependencies: dependencies
  )
  action :create
end