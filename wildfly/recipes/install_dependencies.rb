# encoding: UTF-8
# rubocop:disable LineLength
#
# Cookbook Name:: wildfly
# Recipe:: install_dependencies
#
# Copyright (C) 2014 Brian Dwyer - Intelligent Digital Services
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

wildfly_deploy 'antlr-2.7.7_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/antlr-2.7.7.jar"
end

wildfly_deploy 'aopalliance_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/aopalliance.jar"
end

wildfly_deploy 'aspectjrt_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/aspectjrt.jar"
end

wildfly_deploy 'aspectjtools-1.6.6_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/aspectjtools-1.6.6.jar"
end

wildfly_deploy 'AS400Comm_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/AS400Comm.jar"
end

wildfly_deploy 'axis-1.4_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/axis-1.4.jar"
end

wildfly_deploy 'c3p0-0.9.1.2_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/c3p0-0.9.1.2.jar"
end

wildfly_deploy 'cglib-nodep-2.1_3_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/cglib-nodep-2.1_3.jar"
end

wildfly_deploy 'commons-codec-1.2_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/commons-codec-1.2.jar"
end

wildfly_deploy 'commons-collections-3.2.1_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/commons-collections-3.2.1.jar"
end

wildfly_deploy 'commons-discovery-0.4_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/commons-discovery-0.4.jar"
end

wildfly_deploy 'commons-httpclient-3.1_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/commons-httpclient-3.1.jar"
end

wildfly_deploy 'commons-lang-2.0_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/commons-lang-2.0.jar"
end

wildfly_deploy 'ConsumidorVentaWebService_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/ConsumidorVentaWebService.jar"
end

wildfly_deploy 'guava-r07_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/guava-r07.jar"
end

wildfly_deploy 'hibernate-commons-annotations_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/hibernate-commons-annotations.jar"
end

wildfly_deploy 'hibernate-core_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/hibernate-core.jar"
end

wildfly_deploy 'iogi-0.9.1_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/iogi-0.9.1.jar"
end

wildfly_deploy 'javassist-3.14.0.GA_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/javassist-3.14.0.GA.jar"
end

wildfly_deploy 'jna-4.1.0_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/jna-4.1.0.jar"
end

wildfly_deploy 'jna-platform-4.1.0_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/jna-platform-4.1.0.jar"
end

wildfly_deploy 'jt400_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/jt400.jar"
end

wildfly_deploy 'junit-4.12_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/junit-4.12.jar"
end

wildfly_deploy 'LibSiTef_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/LibSiTef.jar"
end

wildfly_deploy 'log4j-1.2.16_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/log4j-1.2.16.jar"
end

wildfly_deploy 'mail-1.3.3_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/mail-1.3.3.jar"
end

wildfly_deploy 'mirror-1.5.1_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/mirror-1.5.1.jar"
end

wildfly_deploy 'objenesis-1.1_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/objenesis-1.1.jar"
end

wildfly_deploy 'org.springframework.aop-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.aop-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.asm-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.asm-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.aspects-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.aspects-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.beans-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.beans-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.context-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.context-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.context.support-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.context.support-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.core-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.core-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.expression-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.expression-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.instrument-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.instrument-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.instrument.tomcat-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.instrument.tomcat-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.jdbc-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.jdbc-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.jms-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.jms-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.orm-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.orm-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.oxm-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.oxm-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.test-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.test-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.transaction-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.transaction-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.web-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.web-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.web.portlet-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.web.portlet-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.web.servlet-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.web.servlet-3.1.1.RELEASE.jar"
end

wildfly_deploy 'org.springframework.web.struts-3.1.1.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/org.springframework.web.struts-3.1.1.RELEASE.jar"
end

wildfly_deploy 'paranamer-2.2_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/paranamer-2.2.jar"
end

wildfly_deploy 'paypal-core-1.6.2_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/paypal-core-1.6.2.jar"
end

wildfly_deploy 'quartz-1.8.6_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/quartz-1.8.6.jar"
end

wildfly_deploy 'scannotation-1.0.3_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/scannotation-1.0.3.jar"
end

wildfly_deploy 'spring-security-acl-3.0.7.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/spring-security-acl-3.0.7.RELEASE.jar"
end

wildfly_deploy 'spring-security-aspects-3.0.7.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/spring-security-aspects-3.0.7.RELEASE.jar"
end

wildfly_deploy 'spring-security-cas-client-3.0.7.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/spring-security-cas-client-3.0.7.RELEASE.jar"
end

wildfly_deploy 'spring-security-config-3.0.7.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/spring-security-config-3.0.7.RELEASE.jar"
end

wildfly_deploy 'spring-security-core-3.0.7.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/spring-security-core-3.0.7.RELEASE.jar"
end

wildfly_deploy 'spring-security-ldap-3.0.7.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/spring-security-ldap-3.0.7.RELEASE.jar"
end

wildfly_deploy 'spring-security-taglibs-3.0.7.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/spring-security-taglibs-3.0.7.RELEASE.jar"
end

wildfly_deploy 'spring-security-web-3.0.7.RELEASE_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/spring-security-web-3.0.7.RELEASE.jar"
end

wildfly_deploy 'sun.misc.BASE64Decoder_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/sun.misc.BASE64Decoder.jar"
end

wildfly_deploy 'validation-api-1.0.0.GA_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/validation-api-1.0.0.GA.jar"
end

wildfly_deploy 'xpp3_min-1.1.4c_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/xpp3_min-1.1.4c.jar"
end

wildfly_deploy 'xstream-1.3.1_jar' do
  url "https://s3.amazonaws.com/opsworks-library/dependencies-venda-web/xstream-1.3.1.jar"
end