node[:deploy].each do |application, deploy|
  if !deploy['scm'].empty? && deploy['scm']['scm_type'] == 's3'
    jboss_home = node['jboss6']['jboss_home']
    deploy_dir = "#{jboss_home}jboss/server/#{application}/deploy"
    app_context = node['jboss6']['context']['vendaweb'][application]['app_context']
    app_file = deploy['scm']['repository']
    
    Chef::Log.info("Starting deploy - #{application}")
  
    s3_bucket, s3_key = OpsWorks::SCM::S3.parse_uri(app_file)
  
    directory "/tmp/#{application}" do
      mode "0755"
    end
  
    s3_file "/tmp/#{application}/#{app_context}.war" do
      bucket s3_bucket
      remote_path s3_key
      owner "jboss"
      group "jboss"
      mode "0775"
      # per default it's host-style addressing
      # but older versions of rest-client doesn't support host-style addressing with `_` in bucket name
      s3_url "https://s3.amazonaws.com/#{s3_bucket}" if s3_bucket.include?("_")
      action :create
    end

    directory "#{deploy_dir}/#{app_context}.war" do
      owner "jboss"
      group "jboss"
      mode "0755"
      recursive true
      notifies :run, "execute[extract-app]", :immediately
    end

    execute "extract-app" do
      command "cd /tmp/#{application}/; jar xf #{app_context}.war; rm #{app_context}.war; mv * #{deploy_dir}/#{app_context}.war; chown -R jboss:jboss #{deploy_dir}/#{app_context}.war;"
      action :nothing
    end

    Chef::Log.info("Finishing deploy - #{application}")

    service "jboss_#{application}" do
      supports :status => true, :restart => true, :reload => true
      action :restart
    end
  end
end
