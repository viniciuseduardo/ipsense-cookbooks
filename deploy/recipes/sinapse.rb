include_recipe 'deploy::default'

node[:deploy].each do |application, deploy|  
  user_composer_dir = "#{Dir.home(deploy[:user])}/.composer"
  node.default['composer']['home_dir'] = user_composer_dir

  if deploy[:project] != 'sinapse'
    Chef::Log.info("Skipping deploy application #{application} app. Is not Portal Aluno Project")
    next
  end  
  
  if !deploy.has_key?(:scm)
    Chef::Log.info("Skipping deploy application #{application} app")
    next
  end
  
  app_dir = deploy[:deploy_to]  
  Chef::Log.info("Starting deploy - #{application} - #{app_dir}")
  
  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path app_dir
  end  
  
  opsworks_deploy do
    deploy_data deploy
    app application
  end
  
  template "#{deploy[:deploy_to]}/current/config/config-local.php" do
    source "config-local.php.erb"
    mode 0660
    group deploy[:group]
    
    if platform?("ubuntu")
      owner "www-data"
    elsif platform?("amazon")   
      owner "apache"
    end
    
    variables(
      database: deploy[:database],
      variables: (deploy[:environment_variables] rescue {})
    )
  
    only_if { File.directory?("#{deploy[:deploy_to]}/current/config") }
  end  
  
  script "storage_permissions" do
    interpreter "bash"
    user deploy[:user]
    cwd "#{app_dir}/current"
    code <<-EOH
    chmod -R 777 web/assets web/csv
    EOH
    only_if { ::File.exist?("#{app_dir}/current/web") }
  end

  # Install global requirements
  if !node['composer']['global_requirements'].empty?
    execute 'install-composer-global-requirements' do
      user deploy[:user]
      group deploy[:group]
      cwd "#{deploy[:deploy_to]}"
      environment 'COMPOSER_HOME' => user_composer_dir
      command 'composer global require ' + node['composer']['global_requirements'].join(' ') + ' --no-plugins'
    end
  end
  
  execute 'clear cache composer' do
    user deploy[:user]
    group deploy[:group]
    cwd "#{deploy[:deploy_to]}"
    environment 'COMPOSER_HOME' => user_composer_dir
    command 'composer clear-cache'
  end
  
  #Install project dependencies
  composer_project "#{app_dir}/current" do
    quiet false       
    prefer_dist false
    user deploy[:user]
    group deploy[:group]
    action :install
    only_if { ::File.exist?("#{app_dir}/current/composer.json")} 
  end
    
  # Execute init commands
  if deploy.has_key?('init_commands')
    if !deploy['init_commands'].empty?
      deploy['init_commands'].each do |a_command|
        execute a_command do
          user deploy[:user]
          group deploy[:group]
          cwd "#{app_dir}/current/"
          command a_command
          only_if { File.directory?("#{app_dir}/current/vendor") }
        end
      end
    end
  end
  Chef::Log.info("Finishing deploy - #{application}")
end

service "#{node['php']['fpm_package']}" do
  provider(Chef::Provider::Service::Upstart)if (platform?('ubuntu') && node['platform_version'].to_f >= 14.04)
  supports :restart => true
  action [ :enable, :restart ]
  notifies :restart, 'service[nginx]', :immediately
end

service 'nginx' do
  supports :status => true, :restart => true, :reload => true
  action :restart
end