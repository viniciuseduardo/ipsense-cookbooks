include_recipe 'deploy'

node[:deploy].each do |application, deploy|
  if deploy[:project] != 'wordpress'
    Chef::Log.info("Skipping deploy application #{application} app. Is not Gafisa Project")
    next
  end  
  
  if !deploy.has_key?(:scm)
    Chef::Log.info("Skipping deploy application #{application} app. Don't have SCM configuration.")
    next
  end
  
  app_dir = deploy[:deploy_to]
  Chef::Log.info("Starting deploy - #{application} - #{app_dir}")
  
  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path app_dir
  end  
  
  opsworks_deploy do
    deploy_data deploy
    app application
  end
    
  Chef::Log.info("Finishing deploy - #{application}")
end

service "#{node['php']['fpm_package']}" do
  provider(Chef::Provider::Service::Upstart)if (platform?('ubuntu') && node['platform_version'].to_f >= 14.04)
  supports :restart => true
  action [ :enable, :restart ]
  notifies :restart, 'service[nginx]', :immediately
end

service 'nginx' do
  supports :status => true, :restart => true, :reload => true
  action :restart
end