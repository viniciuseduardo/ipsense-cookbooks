include_recipe 'deploy'

node[:deploy].each do |application, deploy|
  if deploy[:project] != 'erp'
    Chef::Log.info("Skipping deploy application #{application} app. Is not Erp Project")
    next
  end  
  
  if !deploy.has_key?(:application_type)
    Chef::Log.info("Skipping deploy application #{application} app")
    next
  end
  
  app_dir = deploy[:deploy_to]  
  Chef::Log.info("Starting deploy - #{application} - #{app_dir}")
  
  opsworks_deploy_dir do
    user deploy[:user]
    group deploy[:group]
    path app_dir
  end  
  
  opsworks_deploy do
    deploy_data deploy
    app application
  end
  
  template "#{app_dir}/current/.env" do
    source ".env.erb"
    mode 0755
    owner deploy[:user]
    group deploy[:group]

    variables(
      database: deploy[:database],
      variables: (deploy[:environment_variables] rescue {}),
    )

    only_if { ::File.directory?("#{app_dir}/current") }
  end

  script "bootstrap_permissions" do
    interpreter "bash"
    user "root"
    cwd "#{app_dir}/current"
    code <<-EOH
    chmod -R 777 bootstrap/cache
    EOH
    only_if { ::File.exist?("#{app_dir}/current/bootstrap/cache") } 
  end

  
  script "storage_permissions" do
    interpreter "bash"
    user "root"
    cwd "#{app_dir}/current"
    code <<-EOH
    chmod -R 777 storage/app storage/framework storage/logs 
    EOH
    only_if { ::File.exist?("#{app_dir}/current/storage") } 
  end
            
  #install project vendors
  composer_project "#{app_dir}/current" do
    quiet false       
    prefer_dist false
    user deploy[:user]
    group deploy[:group]
    action :install
    only_if { ::File.exist?("#{app_dir}/current/composer.json")} 
  end
    
  # Execute init commands
  if deploy.has_key?('init_commands')
    if !deploy['init_commands'].empty?
      deploy['init_commands'].each do |a_command|
        execute a_command do
          user deploy[:user]
          group deploy[:group]
          cwd "#{app_dir}/current/"
          command a_command
          only_if { File.directory?("#{app_dir}/current/vendor") }
        end
      end
    end
  end
  Chef::Log.info("Finishing deploy - #{application}")
end

service "#{node['php']['fpm_package']}" do
  provider(Chef::Provider::Service::Upstart)if (platform?('ubuntu') && node['platform_version'].to_f >= 14.04)
  supports :restart => true
  action [ :enable, :restart ]
  notifies :restart, 'service[nginx]', :immediately
end

service 'nginx' do
  supports :status => true, :restart => true, :reload => true
  action :restart
end