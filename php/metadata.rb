name        "php"
description "Installs PHP"
maintainer  "AWS OpsWorks"
license     "Apache 2.0"
version     "1.0.0"

%w{ apt nginx}.each do |cb|
  depends cb
end