###
# Do not use this file to override the php cookbook's default
# attributes.  Instead, please use the customize.rb attributes file,
# which will keep your adjustments separate from the AWS OpsWorks
# codebase and make it easier to upgrade.
#
# However, you should not edit customize.rb directly. Instead, create
# "php/attributes/customize.rb" in your cookbook repository and
# put the overrides in YOUR customize.rb file.
#
# Do NOT create an 'php/attributes/default.rb' in your cookbooks. Doing so
# would completely override this file and might cause upgrade issues.
#
# See also: http://docs.aws.amazon.com/opsworks/latest/userguide/customizing.html
###

# Define package list
default['php']['version'] = '5'
default['php']['version_full'] = '5.5'
default['php']['apt_repository_ppa'] = 'ppa:ondrej/php'

case node['platform_family']
when 'rhel', 'fedora'
  default['php']['packages'] = %w{ php php-common php-devel php-cli php-pear }
  default['php']['fpm_package'] = 'php-fpm'
else
  default['php']['packages'] = %w{ php5-cgi php5 php5-dev php5-cli php-pear }
  default['php']['fpm_package'] = 'php5-fpm'
end

default['php']['composer']['enable'] = false
default['php']['composer']['github_auth_token'] = ''
default['php']['composer']['dev'] = true
default['php']['composer']['global_requirements'] = []

default['php']['init_commands'] = []

include_attribute "php::customize"