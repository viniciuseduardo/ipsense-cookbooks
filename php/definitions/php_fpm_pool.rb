define :php_fpm_pool do
    pool_name = params[:name]
    
    template "/etc/php/#{node['php']['version_full']}/fpm/pool.d/#{pool_name}.conf" do
      source "php-fpm-pool.conf.erb"
      mode '0640'
      owner user
      group group
      variables({:pool_name => pool_name})
      notifies :restart, "service[#{node['php']['fpm_package']}]"
    end
end
