# Install PHP
if !node['php']['apt_repository_ppa'].nil?
  include_recipe 'apt'
  apt_repository 'repository_php' do
    uri node['php']['apt_repository_ppa']
    distribution node['lsb']['codename']
  end
end


package "#{node['php']['fpm_package']}"

if !node['php']['packages'].nil?
  Chef::Log.info("Install packages: #{node['php']['packages']}")
  node['php']['packages'].each do |a_package|
    package a_package
  end
end

if !node['php']['pools'].nil?
  node['php']['pools'].each do |index, a_pool|
    php_fpm_pool index
  end
end

service "#{node['php']['fpm_package']}" do
  provider Chef::Provider::Service::Upstart
  action :start
end