# Install PHP
if !node['php']['apt_repository_ppa'].empty?
    include_recipe 'apt'
    apt_repository 'repository_php' do
        uri node['php']['apt_repository_ppa']
        distribution node['lsb']['codename']
    end
end

package "php5" do
  action :install
end

Chef::Log.info("Install packages: #{node['php']['packages']}")
if !node['php']['packages'].empty?
    node['php']['packages'].each do |a_package|
        package a_package
    end
end