cookbook_file '/etc/php/5.6/cli/php.ini' do
  source 'php-gafisa.ini'
  owner 'root'
  group 'root'
  mode '0644'
end
service 'nginx' do
  action [:restart]
end
