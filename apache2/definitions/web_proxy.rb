define :web_proxy , :template => 'web_proxy.conf.erb', :app_context => "VendaWeb", :app_port_ajp => "8009" do
  include_recipe "apache2::service"
  domain_name = params[:name][:domain_name] 

  template "#{node[:apache][:dir]}/sites-available/#{domain_name}.conf" do
    Chef::Log.debug("Generating Apache site template for #{domain_name.inspect}")
    source params[:template]
    owner 'root'
    group 'root'
    mode 0644
    variables(
      :domain_name => domain_name,
      :app_context => params[:name][:app_context],
      :app_port_ajp => params[:name][:app_port_ajp]
    )
    if ::File.exists?("#{node[:apache][:dir]}/sites-enabled/#{domain_name}.conf")
      notifies :reload, "service[apache2]", :delayed
    end
  end
  
  apache_site "#{domain_name}.conf" do
    enable enable_setting
  end
end
