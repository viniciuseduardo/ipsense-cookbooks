define :web_proxy , :template => 'web-proxy.conf.erb', :app_context => "VendaWeb", :app_port_http => "8080" do
  domain_name = params[:name][:domain_name] 

  template "#{node[:apache][:dir]}/sites-available/#{domain_name}.conf" do
    Chef::Log.debug("Generating Nginx site template for #{domain_name.inspect}")
    source params[:template]
    owner 'root'
    group node['root_group']
    mode 0644
    variables(
      :domain_name => domain_name,
      :app_context => params[:name][:app_context],
      :app_port_ajp => params[:name][:app_port_http]
    )
    if ::File.exists?("#{node['nginx']['dir']}/sites-enabled/#{domain_name}.conf")
      notifies :reload, 'service[nginx]', :delayed
    end
  end
  
  nginx_site "#{domain_name}.conf"
end
