# Cookbook Name:: jboss6
# Recipe:: default

jboss_home = node['jboss6']['jboss_home']
jboss_user_dir = node['jboss6']['jboss_user_dir']
jboss_user = node['jboss6']['jboss_user']
jboss_archive = node['jboss6']['dl_url'].split('/')[-1].sub!('.tar.gz','')
jboss_install_service = node['jboss6']['install_service']

user jboss_user do
	action :create
	comment "jboss User"
	home "#{jboss_user_dir}#{jboss_user}"
	shell "/bin/bash"
	supports :manage_home => true 
end

remote_file "#{jboss_home}#{jboss_archive}.tar.gz" do
  owner "root"
  group "root"
  mode "0777"
  source node['jboss6']['dl_url']
  notifies :run, "execute[extract-jboss]", :immediately
  not_if { File.exist?("#{jboss_home}#{jboss_archive}.tar.gz") }
end

directory "#{jboss_user_dir}#{jboss_user}" do
	owner jboss_user
	group jboss_user
	mode "0755"
	recursive true
end

execute "extract-jboss" do
	command "cd #{jboss_home}; tar -xvf #{jboss_archive}.tar.gz; chown -R #{jboss_user}:#{jboss_user} #{jboss_user};"
	action :nothing
end

template "#{jboss_home}#{jboss_user}/bin/run.conf" do
	source "run_conf.erb"
  owner jboss_user
  group jboss_user
	mode "0644"
	variables({
      :jvm_min_mem  => node['jboss6']['jvm_min_mem'],
      :jvm_max_mem  => node['jboss6']['jvm_max_mem'],
      :jvm_perm_mem  => node['jboss6']['jvm_perm_mem'],
      :jvm_extra_ops => node['jboss6']['jvm_extra_ops']
    })
end

if jboss_install_service
  template "/etc/init.d/jboss" do
    source "jboss-init-debian.erb"
    mode 0775
    owner "root"
    group "root"
    variables({
        :jboss_home => "#{node['jboss6']['jboss_home']}#{node['jboss6']['jboss_user']}",
        :jboss_user => node['jboss6']['jboss_user'],
        :jboss_bind_ip => node['jboss6']['public_bind_addr'],
        :jboss_conf => "default"
    })
    notifies :start, "service[jboss]"
  end
end