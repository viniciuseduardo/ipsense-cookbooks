define :configure_bindings  do
  app_dir = "#{node['jboss6']['jboss_home']}#{node['jboss6']['jboss_user']}/server/#{params[:app_name]}"

  template "#{app_dir}/conf/bindingservice.beans/META-INF/bindings-jboss-beans.xml" do
    source "bindings-jboss-beans.xml.erb"
    mode 0775
    owner node['jboss6']['jboss_user']
    group node['jboss6']['jboss_user']
    variables({
        :app_port_binding => params[:app_port_binding],
        :app_port_offset => params[:app_port_offset]
    })
  end
end