define :create_context  do  
  jboss_home = node['jboss6']['jboss_home']
  jboss_user = node['jboss6']['jboss_user']
  jboss_conf_default = node['jboss6']['conf_default']
  jboss_dir_default = "#{jboss_home}#{jboss_user}/server/#{jboss_conf_default}"
  
  app_dir = "#{jboss_home}#{jboss_user}/server/#{params[:app_name]}"
  
  execute "create context #{params[:app_name]}" do
    command "cp -R #{jboss_dir_default} #{app_dir}; chown -R #{jboss_user}:#{jboss_user} #{app_dir};"
    not_if { File.exist?("#{app_dir}") }
  end
end
