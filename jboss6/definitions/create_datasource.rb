define :create_datasource, :read_ds => false  do  
  app = params[:app_name]
  app_dir = "#{node['jboss6']['jboss_home']}#{node['jboss6']['jboss_user']}/server/#{app}"
  
  database = node['jboss6']['datasource']
  unless database.nil?
    case database[:type]
    when "mysql"
      connection_url = "jdbc:mysql://#{database['host']}/#{database['database']}"
      template = "datasource/mysql-ds.xml.erb"
    when "oracle-se1"
      connection_url = "jdbc:oracle:thin:@#{database['host']}:#{database['port']}:#{database['database']}"
      template = "datasource/oracle-ds.xml.erb"
    else
      connection_url = ''
      template = ''
    end
        
    template "#{app_dir}/deploy/sco-ds.xml" do
      source template
      mode 0775
      owner "jboss"
      group "jboss"
      variables({
          :ds_name => 'sco',
          :ds_connection_url => connection_url,
          :ds_username => database['username'],
          :ds_password => database['password']
        })
    end

    if(params[:read_ds])
      template "#{app_dir}/deploy/sco_read-ds.xml" do
        source template
        mode 0775
        owner "jboss"
        group "jboss"
        variables({
            :ds_name => 'sco_read',
            :ds_connection_url => connection_url,
            :ds_username => database['username'],
            :ds_password => database['password']
          })
      end      
    end
  end
end