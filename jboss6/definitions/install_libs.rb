define :install_libs  do
  url_lib = params[:dl_libs]
  jboss_home = node['jboss6']['jboss_home']
  jboss_user = node['jboss6']['jboss_user']
  
  app_dir = "#{jboss_home}#{jboss_user}/server/#{params[:app_name]}"
  
  libs_archive = url_lib.split('/')[-1].sub!('.rar','') 
  remote_file "#{app_dir}/lib/#{libs_archive}.rar" do
    owner "root"
    group "root"
    mode "0777"
    source url_lib
  end

  execute "install-libs" do
    command "cd #{app_dir}/lib/; rm -f *.jar; unrar x #{libs_archive}.rar; chown -R #{jboss_user}:#{jboss_user} *.jar; rm -f #{libs_archive}.rar;"
  end
end