name             'jboss6'
maintainer       'Vinicius Alves'
maintainer_email 'viniciuseduardo@ipsense.com.br'
license          'All rights reserved'
description      'Installs/Configures jboss6'
long_description 'Installs/Configures jboss6'
version          '0.0.1'

%w{ apt nginx java newrelic}.each do |cb|
  depends cb
end
