name             'yii2'
description      'Installs and configures PHP application with Nginx, PHP-FPM and MySQL/PostgreSQL'
maintainer       'Ilya Krylov'
maintainer_email 'kiaplayer@gmail.com'
license          'MIT'
version          '1.0.0'

supports         'ubuntu'

%w{
    apt    
    nginx
    composer
}.each do |cb|
    depends cb
end
