#------------------------------------------------------------------------------
# PHP application deploy
#------------------------------------------------------------------------------

node[:deploy].each do |application, deploy|
  node.default['composer']['home_dir'] = "#{::Dir.home(deploy[:user])}/.composer"
    
  opsworks_deploy do
    deploy_data deploy
    app application
  end
  
  template "#{deploy[:deploy_to]}/current/config/config-local.php" do
        source "config-local.php.erb"
        mode 0660
        group deploy[:group]
    
        if platform?("ubuntu")
          owner "www-data"
        elsif platform?("amazon")   
          owner "apache"
        end
    
        variables(
          :host =>     (deploy[:database][:host] rescue nil),
          :port =>     (deploy[:database][:port] rescue nil),
          :user =>     (deploy[:database][:username] rescue nil),
          :password => (deploy[:database][:password] rescue nil),
          :db =>       (deploy[:database][:database] rescue nil)
        )
    
        only_if { File.directory?("#{deploy[:deploy_to]}/current/config") }
    end
    
    # Install project dependencies
    if node['php-app']['composer']['enable']
      execute 'update-composer' do
        command "composer selfupdate"       
      end      
      
      # Install global requirements
      if !node['php-app']['composer']['global_requirements'].empty?
        execute 'install-composer-global-requirements' do
            user deploy[:user]
            group deploy[:group]
            cwd "#{deploy[:deploy_to]}"
            environment ({'HOME' => ::Dir.home(deploy[:user]), 'USER' => deploy[:user]})
            command 'composer global require ' + node['php-app']['composer']['global_requirements'].join(' ') + ' --no-plugins'
        end
      end
      
      execute 'clear cache composer' do
        user deploy[:user]
        group deploy[:group]
        cwd "#{deploy[:deploy_to]}"
        environment ({'HOME' => ::Dir.home(deploy[:user]), 'USER' => deploy[:user]})
        command 'composer clear-cache'
      end 
            
      #install project vendors
      composer_project "#{deploy[:deploy_to]}/current" do
          quiet false       
          prefer_dist false
          user deploy[:user]
          group deploy[:group]
          action :install
      end
    end 
    
    # Execute init commands
    if !node['php-app']['init_commands'].empty?
        node['php-app']['init_commands'].each do |a_command|
            execute a_command do
                user deploy[:user]
                group deploy[:group]
                cwd "#{deploy[:deploy_to]}/current/"
                command a_command
                only_if { File.directory?("#{deploy[:deploy_to]}/current/vendor") }
            end
        end
    end
    
    service 'php5-fpm' do
      provider(Chef::Provider::Service::Upstart)if (platform?('ubuntu') && node['platform_version'].to_f >= 14.04)
      supports :restart => true
      action [ :enable, :restart ]
      notifies :restart, 'service[nginx]', :immediately
    end
    
    service 'nginx' do
      supports :status => true, :restart => true, :reload => true
      action :restart
    end    
end