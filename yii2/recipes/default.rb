#------------------------------------------------------------------------------
# PHP application installation
#------------------------------------------------------------------------------
include_recipe 'nginx'

# Add application user and group
app_user = node['php-app']['user']
app_group = node['php-app']['group']
app_homedir = "/app"

group app_group do
    append true
    action :create
end

user app_user do
    supports :manage_home => true
    gid app_group
    home app_homedir
    shell '/bin/bash'
    action :create
end

# Create folders
[ node['php-app']['project_dir'], node['php-app']['log_dir'] ].each do |a_directory|
    directory a_directory do
        owner app_user
        group node['nginx']['group']
        mode '0750'
        recursive true
        action :create
    end
end

ssh_dir = "#{app_homedir}/.ssh"

directory ssh_dir do
    owner app_user
    group app_group
    mode '0700'
    action :create
end

# Install PHP
if !node['php-app']['php']['apt_repository_ppa'].empty?
    include_recipe 'apt'
    apt_repository 'repository_php' do
        uri node['php-app']['php']['apt_repository_ppa']
        distribution node['lsb']['codename']
        components ['main']
        keyserver 'keyserver.ubuntu.com'
        key node['php-app']['php']['apt_repository_key']
    end
end

package 'php5-fpm'

service 'php5-fpm' do
    provider Chef::Provider::Service::Upstart
    action :nothing
end

if !node['php-app']['php']['packages'].empty?
    node['php-app']['php']['packages'].each do |a_package|
        package a_package
    end
end

# Set custom PHP settings

if !node['php-app']['php']['custom_directives'].empty?
    module_name = node['php-app']['php']['custom_module_name'];
    execute 'enable-custom-php-module' do
        command "php5enmod #{module_name}"
        action :nothing
        notifies :restart, 'service[php5-fpm]'
    end
    custom_module_content = ''
    node['php-app']['php']['custom_directives'].each do |key, value|
        custom_module_content += "#{key} = #{value}\n"
    end
    file "/etc/php5/mods-available/#{module_name}.ini" do
        content custom_module_content
        owner 'root'
        group 'root'
        mode '0644'
        action :create
        notifies :run, 'execute[enable-custom-php-module]'
    end
end

# Create PHP-FPM pools

if !node['php-app']['php']['pools'].empty?
    node['php-app']['php']['pools'].each do |a_pool|
        template "/etc/php5/fpm/pool.d/#{a_pool['name']}.conf" do
            source a_pool['template']
            local a_pool['template_local']
            mode '0640'
            owner app_user
            group node['nginx']['group']
            variables(a_pool['variables'])
            notifies :restart, 'service[php5-fpm]'
        end
    end
end

# Create NGINX vhosts

if !node['php-app']['vhosts'].empty?
    node['php-app']['vhosts'].each do |a_vhost|
        vhost_filename = "#{a_vhost['name']}.conf"
        template "/etc/nginx/sites-available/#{vhost_filename}" do
            source a_vhost['template']
            local a_vhost['template_local']
            mode '0600'
            owner app_user
            group app_group
            variables(a_vhost['variables'])
            notifies :reload, 'service[nginx]', :delayed
        end
        nginx_site vhost_filename
    end
end


# Install Composer

if node['php-app']['composer']['enable']
    include_recipe 'composer::default'

    # Install global requirements
    if !node['php-app']['composer']['global_requirements'].empty?
        execute 'install-composer-global-requirements' do
            user app_user
            group app_group
            cwd app_homedir
            environment ({'HOME' => ::Dir.home(app_user), 'USER' => app_user})
            command 'composer global require ' + node['php-app']['composer']['global_requirements'].join(' ') + ' --no-plugins'
        end
    end
end